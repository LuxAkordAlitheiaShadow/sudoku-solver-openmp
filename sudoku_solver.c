//
// Created by Titouan 'LuX' Allain on 12/6/21.
//
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <omp.h>


void reduce_possibilities(int*** sudoku_grid, int grid_size);

int getting_grid_size(char* file_name)
{
    FILE* fp=fopen(file_name, "r");
    if (fp==NULL){
        printf("Error, file cannot opened.");
        // TODO Exit FAILURE
    }

    int grid_size = 0;
    char character;
    while ( (character = fgetc(fp)) != EOF )
    {
        // End of line statement
        if ( character == '\n' )
        {
            grid_size++;
        }
    }
    return grid_size;
}

void read_file(char* fileName, int*** sudoku_grid, int grid_size)
{
    FILE* fp=fopen(fileName, "r");
    if (fp==NULL){
        printf("Error, file cannot opened.");
        // TODO Exit FAILURE
    }

    int line, elem_in_line, possible_elem;

    // Initializing array
    for (line = 0; line < grid_size; line++)
    {
        for (elem_in_line = 0; elem_in_line < grid_size; elem_in_line++)
        {
            // Copying data to array
            fscanf(fp, "%d\t", &sudoku_grid[line][elem_in_line][0]);
            // Initializing all possibilities
            for (possible_elem = 1 ; possible_elem < grid_size ; possible_elem++)
            {
                sudoku_grid[line][elem_in_line][possible_elem] = 0;
            }
        }
    }
    fclose(fp);
}

int is_in_line(int number_to_fill, int*** sudoku_grid, int grid_size, int line)
{
    int elem_in_line;
    int in_line = 0;

#pragma omp parallel for
    for (elem_in_line = 0 ; elem_in_line < grid_size ; elem_in_line++)
    {
        // Number to fill match in line statement
        if (sudoku_grid[line][elem_in_line][0] == number_to_fill)
        {
            in_line = 1;
        }
    }

    // Number is in line statement
    if ( in_line )
    {
        return 1; // Return true
    }
        // Number isn't in line statement
    else
    {
        return 0; // Return false
    }
}

int is_in_column(int number_to_fill, int*** sudoku_grid, int grid_size, int elem_in_line)
{
    int line;
    int in_column = 0;

#pragma omp parallel for
    for (line = 0 ; line < grid_size ; line++)
    {
        // Number to fill match in column statement
        if (sudoku_grid[line][elem_in_line][0] == number_to_fill)
        {
            in_column = 1;
        }
    }

    // Number is in column statement
    if ( in_column )
    {
        return 1; // Return true
    }
        // Number isn't in column statement
    else
    {
        return 0; // Return false
    }
}

int is_in_square(int number_to_fill, int*** sudoku_grid, int grid_size, int line, int elem_in_line)
{
    int sudoku_size = grid_size;
    int square_size = sqrt(sudoku_size);

    int elem_in_line_range_multiplier = elem_in_line / square_size;
    int elem_in_line_min = (elem_in_line_range_multiplier + 1) * square_size - square_size;
    int elem_in_line_max = (elem_in_line_range_multiplier + 1) * square_size;

    int line_range_multiplier = line / square_size;
    int line_min = (line_range_multiplier + 1) * square_size - square_size;
    int line_max = (line_range_multiplier + 1) * square_size;

    int in_square = 0;

#pragma omp parallel for private(elem_in_line)
    for (line = line_min ; line < line_max ; line++)
    {
#pragma omp parallel for
        for (elem_in_line = elem_in_line_min ; elem_in_line < elem_in_line_max ; elem_in_line++)
        {
            // Number to fill match in square statement
            if (sudoku_grid[line][elem_in_line][0] == number_to_fill)
            {
                in_square = 1;
            }
        }
    }

    // Number is in square statement
    if ( in_square )
    {
        return 1; // Return true
    }
        // Number isn't in square statement
    else
    {
        return 0; // Return false
    }
}

void print_grid(int*** sudoku_grid, int grid_size)
{
    int line, elem_in_line;

    for (line = 0 ; line < grid_size ; line++)
    {
        for (elem_in_line = 0 ; elem_in_line < grid_size ; elem_in_line++)
        {
            printf("%d ", sudoku_grid[line][elem_in_line][0]);
        }
        printf("\n");
    }
}

void enumerate_possibilities(int*** sudoku_grid, int grid_size)
{
    int line, elem_in_line, possible_elem;
    int number_to_fill;
    int in_line, in_column, in_square;
    for (line = 0 ; line < grid_size ; line++)
    {
        for (elem_in_line = 0; elem_in_line < grid_size; elem_in_line++) {
            // Element need to be solved statement
            if (sudoku_grid[line][elem_in_line][0] == 0) {
                // Checking if number is valid using parallelization
                for (number_to_fill = 1; number_to_fill <= grid_size; number_to_fill++)
                {
                    in_line = 0;
                    in_column = 0;
                    in_square = 0;
#pragma omp parallel sections
                    {
                        // Number is in line statement
                        if (is_in_line(number_to_fill, sudoku_grid, grid_size, line)) {
                            in_line = 1;
                        }
#pragma omp section
                        {
                            // Number is in column statement
                            if (is_in_column(number_to_fill, sudoku_grid, grid_size, elem_in_line)) {
                                in_column = 1;
                            }
                        }
#pragma omp section
                        {
                            // Number is in square statement
                            if (is_in_square(number_to_fill, sudoku_grid, grid_size, line, elem_in_line)) {
                                in_square = 1;
                            }
                        }
                    }
                    // Number valid statement
                    if (!in_line & !in_column && !in_square) {

                        for (possible_elem = 1; possible_elem < grid_size; possible_elem++) {
                            // Possible element is empty statement
                            if (sudoku_grid[line][elem_in_line][possible_elem] == 0) {
                                // Add number to possible value of the case
                                sudoku_grid[line][elem_in_line][possible_elem] = number_to_fill;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}

void remove_in_line(int*** sudoku_grid, int grid_size, int elem_in_line, int number_to_remove)
{
    int line, possible_elem;
#pragma omp parallel for private(possible_elem)
    for ( line = 0 ; line < grid_size ; line++)
    {
#pragma omp parallel for
        for ( possible_elem = 1 ; possible_elem < grid_size ; possible_elem++ )
        {
            // Number filled is a possibility statement
            if ( sudoku_grid[line][elem_in_line][possible_elem] == number_to_remove )
            {
                // Removing already filled number
                sudoku_grid[line][elem_in_line][possible_elem] = 0;
            }
        }
    }
}

void remove_in_column(int*** sudoku_grid, int grid_size, int line, int number_to_remove)
{
    int elem_of_line, possible_elem;
#pragma omp parallel for private(possible_elem)
    for ( elem_of_line = 0 ; elem_of_line < grid_size ; elem_of_line++)
    {
#pragma omp parallel for
        for ( possible_elem = 1 ; possible_elem < grid_size ; possible_elem++ )
        {
            // Number filled is a possibility statement
            if ( sudoku_grid[line][elem_of_line][possible_elem] == number_to_remove )
            {
                // Removing already filled number
                sudoku_grid[line][elem_of_line][possible_elem] = 0;
            }
        }
    }
}

void remove_in_square(int*** sudoku_grid, int grid_size, int line, int elem_in_line, int number_to_remove)
{
    int possible_elem;
    //TODO use is_in??
    int square_size = sqrt(grid_size);

    int elem_in_line_range_multiplier = elem_in_line / square_size;
    int elem_in_line_min = (elem_in_line_range_multiplier + 1) * square_size - square_size;
    int elem_in_line_max = (elem_in_line_range_multiplier + 1) * square_size;

    int line_range_multiplier = line / square_size;
    int line_min = (line_range_multiplier + 1) * square_size - square_size;
    int line_max = (line_range_multiplier + 1) * square_size;

#pragma omp parallel for private(elem_in_line, possible_elem)
    for (line = line_min ; line < line_max ; line++)
    {
#pragma omp parallel for private(possible_elem)
        for (elem_in_line = elem_in_line_min ; elem_in_line < elem_in_line_max ; elem_in_line++)
        {
#pragma omp parallel for
            for( possible_elem = 1 ; possible_elem < grid_size ; possible_elem++ )
            {
                // Number to fill match in square statement
                if (sudoku_grid[line][elem_in_line][possible_elem] == number_to_remove)
                {
                    // Removing already filled number
                    sudoku_grid[line][elem_in_line][possible_elem] = 0;
                }
            }

        }
    }
}

void remove_possibilities(int*** sudoku_grid, int grid_size, int line, int elem_in_line)
{
    int possible_elem;
    for ( possible_elem = 1 ; possible_elem < grid_size ; possible_elem++ )
    {
        // Removing possibility
        sudoku_grid[line][elem_in_line][possible_elem] = 0;
    }
}

void remove_number(int*** sudoku_grid, int grid_size, int line, int elem_in_line, int number_to_remove)
{
#pragma omp parallel sections
    {
        // Removing in line
        remove_in_line(sudoku_grid, grid_size, elem_in_line, number_to_remove);
#pragma omp section
        {
            // Removing in column
            remove_in_column(sudoku_grid, grid_size, line, number_to_remove);
        }
#pragma omp section
        {
            // Removing in square
            remove_in_square(sudoku_grid, grid_size, line, elem_in_line, number_to_remove);
        }
#pragma omp section
        {
            remove_possibilities(sudoku_grid, grid_size, line, elem_in_line);
        }
    }
}

int is_unique_in_possibilities(int*** sudoku_grid, int grid_size, int line, int elem_in_line)
{
    int possible_elem;
    int number_of_possibilities = 0;

    for (possible_elem = 1 ; possible_elem < grid_size ; possible_elem++ )
    {
        // Is a possibility statement
        if ( sudoku_grid[line][elem_in_line][possible_elem] != 0 )
        {
            number_of_possibilities++;
        }
        // Case have many possibilities statement
        if ( number_of_possibilities > 1 )
        {
            return 0; // Return false
        }
    }
    return 1; // Return true
}

int get_position_of_unique_possibility(int*** sudoku_grid, int grid_size, int line, int elem_in_line)
{
    int possible_elem;
    for ( possible_elem = 1 ; possible_elem < grid_size ; possible_elem++ )
    {
        // Is the possibility statement
        if ( sudoku_grid[line][elem_in_line][possible_elem] != 0 )
        {
            return possible_elem;
        }
    }
    return 0; // Error return
}

int possibility_in_line(int*** sudoku_grid, int grid_size, int line, int elem_in_line_for_number_to_check, int number_to_check)
{
    int elem_in_line, possible_elem;

    for ( elem_in_line = 0 ; elem_in_line < grid_size ; elem_in_line++ )
    {
        // Checking another column statement
        if ( elem_in_line != elem_in_line_for_number_to_check )
        {
            // Another case isn't fill statement
            if ( sudoku_grid[line][elem_in_line][0] == 0 )
            {
                for (possible_elem = 1; possible_elem < grid_size; possible_elem++)
                {
                    // Number to check is a possibility for another case in line statement
                    if ( sudoku_grid[line][elem_in_line][possible_elem] == number_to_check )
                    {
                        return 1; // Return true
                    }
                }
            }
        }
    }
    return 0; // Return false
}

int possibility_in_column(int*** sudoku_grid, int grid_size, int line_for_number_to_check, int elem_in_line, int number_to_check)
{
    int line, possible_elem;
    for ( line = 0 ; line < grid_size ; line++ )
    {
        // Checking another column statement
        if ( line != line_for_number_to_check )
        {
            // Another case isn't fill statement
            if ( sudoku_grid[line][elem_in_line][0] == 0 )
            {
                for (possible_elem = 1; possible_elem < grid_size; possible_elem++)
                {
                    // Number to check is a possibility for another case in column statement
                    if ( sudoku_grid[line][elem_in_line][possible_elem] == number_to_check )
                    {
                        return 1; // Return true
                    }
                }
            }
        }
    }
    return 0; // Return false
}

int possibility_in_square(int*** sudoku_grid, int grid_size, int line_for_number_to_check, int elem_in_line_for_number_to_check, int number_to_check)
{
    int line, elem_in_line, possible_elem;
    //TODO use is_in??
    int square_size = sqrt(grid_size);

    int elem_in_line_range_multiplier = elem_in_line_for_number_to_check / square_size;
    int elem_in_line_min = (elem_in_line_range_multiplier + 1) * square_size - square_size;
    int elem_in_line_max = (elem_in_line_range_multiplier + 1) * square_size;

    int line_range_multiplier = line_for_number_to_check / square_size;
    int line_min = (line_range_multiplier + 1) * square_size - square_size;
    int line_max = (line_range_multiplier + 1) * square_size;

    for (line = line_min ; line < line_max ; line++)
    {
        for (elem_in_line = elem_in_line_min ; elem_in_line < elem_in_line_max ; elem_in_line++)
        {
            // Checking another case statement
            if ( !(line == line_for_number_to_check && elem_in_line == elem_in_line_for_number_to_check) )
            {
                // Another case isn't fill statement
                if ( sudoku_grid[line][elem_in_line][0] == 0 )
                {
                    for( possible_elem = 1 ; possible_elem < grid_size ; possible_elem++ )
                    {
                        // Number to fill match in square statement
                        if (sudoku_grid[line][elem_in_line][possible_elem] == number_to_check)
                        {
                            return 1; // Return true
                        }
                    }
                }
            }
        }
    }
    return 0; // Return false
}

int sudoku_is_fill(int*** sudoku_grid, int grid_size)
{
    int line, elem_in_line;
    int is_fill = 1;
#pragma omp parallel for private(elem_in_line)
    for ( line = 0 ; line < grid_size ; line++)
    {
#pragma omp parallel for
        for ( elem_in_line = 0 ; elem_in_line < grid_size ; elem_in_line++ )
        {
            // Case isn't fill statement
            if ( sudoku_grid[line][elem_in_line][0] == 0)
            {
                is_fill = 0;
            }
        }
    }
    // Sudoku isn't fill statement
    if ( !is_fill )
    {
        return 0; // Return false
    }
        // Sudoku is fill statement
    else
    {
        return 1; // Return true
    }
}

void release_grid(int*** sudoku_grid, int grid_size)
{
    int line, elem_in_line;

#pragma omp parallel for private(elem_in_line)
    for ( line = 0; line < grid_size; line++)
    {
#pragma omp parallel for
        for ( elem_in_line = 0 ; elem_in_line < grid_size ; elem_in_line++ )
        {
            free(sudoku_grid[line][elem_in_line]);
        }
        free(sudoku_grid[line]);
    }
    free(sudoku_grid);
}

void backtracking (int*** original_sudoku_grid, int grid_size)
{
    int line, elem_in_line, possible_elem;

    // Copying and allocating copy of original grid
    int*** copied_sudoku_grid;
    copied_sudoku_grid = malloc(grid_size * sizeof(int**));
#pragma omp parallel for private(elem_in_line, possible_elem)
    for ( line = 0 ; line < grid_size ; line++ )
    {
        copied_sudoku_grid[line] = malloc(grid_size * sizeof(int*));
#pragma omp parallel for private(possible_elem)
        for ( elem_in_line = 0 ; elem_in_line < grid_size ; elem_in_line++ )
        {
            copied_sudoku_grid[line][elem_in_line] = malloc(grid_size * sizeof(int));
#pragma omp parallel for
            for ( possible_elem = 0 ; possible_elem < grid_size ; possible_elem++ )
            {
                copied_sudoku_grid[line][elem_in_line][possible_elem] = original_sudoku_grid[line][elem_in_line][possible_elem];
            }
        }
    }
    printf("BACKTRACK\n");
    print_grid(copied_sudoku_grid, grid_size);
    // Choose possibility to try
    int have_choose_possibility = 0;
    for ( line = 0 ; line < grid_size ; line++ )
    {
        for ( elem_in_line = 0 ; elem_in_line < grid_size ; elem_in_line++ )
        {
            // Case isn't fill statement
            if ( copied_sudoku_grid[line][elem_in_line][0] == 0 )
            {
                for ( possible_elem = 1 ; possible_elem < grid_size ; possible_elem++ )
                {
                    // Is a possibility statement
                    if ( copied_sudoku_grid[line][elem_in_line][possible_elem] != 0 )
                    {
                        // Possibility is valid statement
                        if ( !is_in_line(copied_sudoku_grid[line][elem_in_line][possible_elem], copied_sudoku_grid, grid_size, line) && !is_in_column(copied_sudoku_grid[line][elem_in_line][possible_elem], copied_sudoku_grid, grid_size, elem_in_line) && !is_in_square(copied_sudoku_grid[line][elem_in_line][possible_elem], copied_sudoku_grid, grid_size, line, elem_in_line) )
                        {
                            copied_sudoku_grid[line][elem_in_line][0] = copied_sudoku_grid[line][elem_in_line][possible_elem];
                            remove_number(copied_sudoku_grid, grid_size, line, elem_in_line, copied_sudoku_grid[line][elem_in_line][0]);
                            printf("Possibility chosen [%d : %d](%d) : %d\n",line, elem_in_line, possible_elem, copied_sudoku_grid[line][elem_in_line][0]);
                            have_choose_possibility = 1;
                            break;
                        }
                    }
                }
            }
            // Possibility chosen statement
            if ( have_choose_possibility == 1 )
            {
                break;
            }
        }
        // Possibility chosen statement
        if ( have_choose_possibility == 1 )
        {
            break;
        }
    }
    // Possibility chosen statement
    if ( have_choose_possibility == 1)
    {
        // Reduce possibilities
        reduce_possibilities(copied_sudoku_grid, grid_size);
    }
        // Possibility haven't chosen  statement
    else
    {
        // CALLBACK
        printf("CALLBACK");
        int fuck;
        scanf("%d", &fuck);
    }

    // Copying to original grid
    for ( line = 0 ; line < grid_size ; line++ )
    {
        for ( elem_in_line = 0 ; elem_in_line < grid_size ; elem_in_line++ )
        {
            original_sudoku_grid[line][elem_in_line][0] = copied_sudoku_grid[line][elem_in_line][0];
        }
    }

    // Releasing copied grid
    release_grid(copied_sudoku_grid, grid_size);
}

void validating_number(int*** sudoku_grid, int grid_size, int line, int elem_in_line, int possible_elem, int valid_number)
{
    // Validating number for this case
    sudoku_grid[line][elem_in_line][0] = sudoku_grid[line][elem_in_line][possible_elem];
    // Removing validated number
    remove_number(sudoku_grid, grid_size, line, elem_in_line, valid_number);
}

void reduce_possibilities(int*** sudoku_grid, int grid_size)
{
    int line, elem_in_line, possible_elem;
    int have_reduce_possibility = 1;
    int it = 0;

    while ( have_reduce_possibility )
    {
        have_reduce_possibility = 0;
        it++;
        printf("It %d\n", it);
        print_grid(sudoku_grid, grid_size);

        for ( line = 0 ; line < grid_size ; line++)
        {
            for ( elem_in_line = 0 ; elem_in_line < grid_size ; elem_in_line++)
            {
                // Case is not fill statement
                if ( sudoku_grid[line][elem_in_line][0] == 0 )
                {
                    // Case have one possibility statement
                    if (is_unique_in_possibilities(sudoku_grid, grid_size, line, elem_in_line) )
                    {
                        validating_number(sudoku_grid, grid_size, line, elem_in_line, get_position_of_unique_possibility(sudoku_grid, grid_size, line, elem_in_line), sudoku_grid[line][elem_in_line][get_position_of_unique_possibility(sudoku_grid, grid_size, line, elem_in_line)]);
                        have_reduce_possibility = 1;
                    }
                        // Case have many possibilities statement
                    else
                    {
                        for ( possible_elem = 1; possible_elem < grid_size; possible_elem++ )
                        {
                            // Number to check isn't a possibility of another case in line statement
                            if ( !possibility_in_line(sudoku_grid, grid_size, line, elem_in_line,sudoku_grid[line][elem_in_line][possible_elem]) )
                            {
                                validating_number(sudoku_grid, grid_size, line, elem_in_line, possible_elem, sudoku_grid[line][elem_in_line][possible_elem]);
                                have_reduce_possibility = 1;
                                break;
                            }
                                // Number to check isn't a possibility of another case in column statement
                            else if ( !possibility_in_column(sudoku_grid, grid_size, line, elem_in_line, sudoku_grid[line][elem_in_line][possible_elem]) )
                            {
                                validating_number(sudoku_grid, grid_size, line, elem_in_line, possible_elem, sudoku_grid[line][elem_in_line][possible_elem]);
                                have_reduce_possibility = 1;
                                break;
                            }
                                // Number to check isn't a possibility of another case in square statement
                            else if ( !possibility_in_square(sudoku_grid, grid_size, line, elem_in_line, sudoku_grid[line][elem_in_line][possible_elem]))
                            {
                                validating_number(sudoku_grid, grid_size, line, elem_in_line, possible_elem, sudoku_grid[line][elem_in_line][possible_elem]);
                                have_reduce_possibility = 1;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    // Sudoku isn't fill statement
    if ( !(sudoku_is_fill(sudoku_grid, grid_size)) )
    {
        // Starting backtracking method
        backtracking(sudoku_grid, grid_size);
    }
}

int main(int argc, char** argv)
{
    // TODO need to check cmd arg for matching teacher cmd ?
    //mpirun --npernode 1 -np 16 a.out < grille.txt

    // Getting size of grid
    int grid_size;
    grid_size = getting_grid_size("sudoku3");

    // Declaring and allocating grid
    int*** sudoku_grid;
    int line, elem_in_line;
    sudoku_grid = malloc(grid_size* sizeof(int**));
    #pragma omp parallel for private(elem_in_line)
    for (line = 0 ; line < grid_size ; line++)
    {
        sudoku_grid[line] = malloc(grid_size * sizeof(int*));
        #pragma omp parallel for
        for (elem_in_line = 0 ; elem_in_line < grid_size ; elem_in_line++)
        {
            sudoku_grid[line][elem_in_line] = malloc(grid_size * sizeof(int));
        }
    }

    // Reading file
    read_file("sudoku3", sudoku_grid, grid_size);

    // Printing unsolved sudoku
    print_grid(sudoku_grid, grid_size);

    // Getting all possibilities for first iteration
    enumerate_possibilities(sudoku_grid, grid_size);

//    // Printing sudoku with all possibilities
//    printf("\n");
//    for (line = 0 ; line < grid_size ; line++)
//    {
//        for (elem_in_line = 0 ; elem_in_line < grid_size ; elem_in_line++)
//        {
//            printf("{");
//            for (possible_elem = 0 ; possible_elem < grid_size ; possible_elem++)
//            {
//                printf("%d, ", sudoku_grid[line][elem_in_line][possible_elem]);
//            }
//            printf("}");
//        }
//        printf("\n\n");
//    }

    // Reduce possibilities
    reduce_possibilities(sudoku_grid, grid_size);

    // Printing result
    printf("\n\n\n");
    print_grid(sudoku_grid, grid_size);

    // Releasing sudoku grid
    release_grid(sudoku_grid, grid_size);

    return 0; // Exit with success
}